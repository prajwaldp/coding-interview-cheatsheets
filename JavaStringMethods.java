import java.util.Arrays;

public class JavaStringMethods {
  public static void main(String[] args) {
    String joined = String.join(", ", Arrays.asList("Hello", "World!"));
    System.out.println(joined);

    String repeated = "repeated".repeat(10);
    System.out.println(repeated);
  }
}
